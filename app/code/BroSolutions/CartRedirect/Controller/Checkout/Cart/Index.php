<?php

namespace BroSolutions\CartRedirect\Controller\Checkout\Cart;

class Index extends \Magento\Checkout\Controller\Cart\Index
{

    public function execute()
    {
        parent::execute();

        return $this->resultRedirectFactory->create()->setPath('checkout');
    }
}

