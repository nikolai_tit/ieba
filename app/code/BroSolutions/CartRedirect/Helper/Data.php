<?php

namespace BroSolutions\CartRedirect\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }

    public function isCouponHidden()
    {
        return (boolean)$this->_scopeConfig->getValue('couponsetting/general/disable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}