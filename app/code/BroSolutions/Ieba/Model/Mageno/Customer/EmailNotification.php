<?php
/**
 * Created by PhpStorm.
 * User: furman
 * Date: 23.12.17
 * Time: 17:31
 */

namespace BroSolutions\Ieba\Model\Mageno\Customer;

class EmailNotification extends \Magento\Customer\Model\EmailNotification
{
    public function newAccount(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $type = self::NEW_ACCOUNT_EMAIL_REGISTERED,
        $backUrl = '',
        $storeId = 0,
        $sendemailStoreId = null
    ) {
        return null;
    }
}
